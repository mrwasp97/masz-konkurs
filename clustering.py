import networkx as nx
import matplotlib.pyplot as plt


def enron_graph():
    with open('ia-enron-only.mtx') as file:
        lines = file.readlines()[2:]
    g = nx.empty_graph()
    for line in lines:
        temp = line.split(' ')
        from_node = int(temp[0])
        to_node = int(temp[1])
        g.add_edge(from_node, to_node)
    return g


def block_graph(clusters_count: int, cluster_size: int, inside_prob: float, outside_prob: float):
    cluster_sizes = [cluster_size for _ in range(clusters_count)]
    probs = [[inside_prob if i == j else outside_prob for i in range(clusters_count)] for j in range(clusters_count)]
    g = nx.stochastic_block_model(cluster_sizes, probs, seed=2137)
    return g


def get_graph():
    sizes = [30, 30, 30]
    probs = [[0.50, 0.02, 0.02], [0.02, 0.50, 0.02], [0.02, 0.02, 0.50]]
    graph = nx.stochastic_block_model(sizes, probs, seed=2137)
    return graph


def edge_with_highest_betweenness(g):
    edge_betweennesses = nx.edge_betweenness_centrality(g)
    sorted_betweennesses = sorted(edge_betweennesses.items(), key=lambda key_value: key_value[1], reverse=True)
    return sorted_betweennesses[0][0]


def girvan_newman_clustering(graph, clusters=3):
    graph = graph.copy()
    components = nx.connected_components(graph)
    components_count = nx.number_connected_components(graph)

    while components_count < clusters:
        # STEP 1, 3
        edge_to_remove = edge_with_highest_betweenness(graph)
        # STEP 2
        graph.remove_edge(edge_to_remove[0], edge_to_remove[1])
        components = nx.connected_components(graph)
        # STEP 4
        components_count = nx.number_connected_components(graph)

    return [list(component) for component in components]


def print_clusters(graph, clusters):
    colors = ['red', 'green', 'blue', 'purple', 'yellow', 'gray']
    color_map = []
    for vertex in graph:
        for i, cluster in enumerate(clusters):
            if vertex in cluster:
                color_map.append(colors[i])
    nx.draw_kamada_kawai(graph, node_color=color_map, with_labels=True)
    plt.show()


def test_clustering(graph, clusters):
    nx.draw_kamada_kawai(graph, with_labels=True)
    plt.show()
    clusters = girvan_newman_clustering(graph, clusters)
    print_clusters(graph, clusters)


test_clustering(block_graph(3, 30, inside_prob=0.5, outside_prob=0.02), 3)
test_clustering(block_graph(5, 30, inside_prob=0.5, outside_prob=0.05), 5)
test_clustering(block_graph(5, 30, inside_prob=0.5, outside_prob=0.05), 3)
test_clustering(nx.karate_club_graph(), 2)
test_clustering(enron_graph(), 5)
